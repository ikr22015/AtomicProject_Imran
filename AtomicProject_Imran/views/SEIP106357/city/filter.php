<?php
	include_once("../../../vendor/autoload.php");
	
	use Imran\BITM\SEIP106357\city\Select;
	use Imran\BITM\SEIP106357\Utility\Utility;
	
	$myCity = new Select();
	$cities = $myCity->filter($_REQUEST['filter']);
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>City</title>
	<link rel="stylesheet" href="../../../resource/css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">City</a></h1>
			<nav class="navbar navbar-inverse">
				<ul>
					<li><a class="active" href="index.php">Home</a></li>
					<li><a href="../../../index.php">Go to Ptoject Navigation Page</a></li>
					<li><a href="#">Click here to view the source code</a></li>
				</ul>
			</nav>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="booklist-sub-container">
				<div class="list-nav">
					<ul>
						<li>
							<form action="filter.php" method="get">
								<input type="number" name="filter" placeholder="Search by ID...">
								<button type="submit" name="" value="">Search</button>
							</form>
						</li>
						<li><a href="">Download</a></li>
						<li><a href="create.php">Add New Book</a></li>
					</ul>
				</div>
				<div id="profile-sum-org-panel-success" class="panel panel-primary">
					<div id="profile-sum-org-panel-heading" class="panel-heading">
						<p style="margin: 0;">City List</p>
					</div>
					<div>
						<?php echo Utility::message(); ?>
					</div>
						<table class="table table-bordered" border="1">
						  <tr>
							<th style="text-align:center">ID</th>
							<th style="text-align:center">Name</th> 
							<th style="text-align:center">City</th> 
							<th style="text-align:center">Action</th>
						  </tr>
						  
						  <tr>
							<td style="text-align:left"><?php echo $cities->id ?></td> 
							<td style="text-align:left"><?php echo $cities->title ?></td> 
							<td style="text-align:left"><?php echo $cities->location ?></td> 
							<td style="text-align:center">
								<button type="button" name="button" value=""><a href="show.php?id=<?php echo $cities->id ?>">View</a></button>
								<button type="button" name="button" value=""><a href="edit.php?id=<?php echo $cities->id ?>">Edit</a></button>
								<form style="display:inline-block" action="delete.php" method="post">
								<input type="hidden" name="id" value="<?php echo $cities->id ?>">
								<button type="button" name="button" value=""><a href="delete.php?id=<?php echo $cities->id ?>">Delete</a></button>
								</form>
							</td>
						  </tr>
						</table>
				</div>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
