<?php
	include_once("../../../vendor/autoload.php");
	
	use Imran\BITM\SEIP106357\Hobbies\CheckboxMultiple;
	use Imran\BITM\SEIP106357\Utility\Utility;
	
	$myHobbies = new CheckboxMultiple();
	$myHobby = $myHobbies->show($_REQUEST['id']);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Hobby</title>
	<link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">Hobby</a></h1> 
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="add-gender-sub-container">
				<h2>Add My Hobbies</h2>
				<form action="update.php" method="post">
						<label>Enter Your Name</label>
						<input style="padding-left:5px;margin-left:7px" type="text" name="name" placeholder="Name" value="<?php echo $myHobby->title?>" /></br>
						<label>Choose your hobbies.</label></br>
						<table class="table table-condensed">
							<tr class="active">
								<td><input type="checkbox" name="football" value="Football"/>Football</td>
								<td><input type="checkbox" name="chees" value="Chees"/>Chees</td>		
								<td><input type="checkbox" name="cricket" value="Cricket"/>Cricket</td>
							</tr>
							<tr class="success">
								<td><input type="checkbox" name="watching-tv" value="Watching TV"/>Watching TV</td>
								<td><input type="checkbox" name="family-time" value="Family Time"/>Family Time</td>		
								<td><input type="checkbox" name="fishing" value="Fishing"/>Fishing</td>
							</tr>
							<tr class="active">
								<td><input type="checkbox" name="computer" value="Computer"/>Computer</td>
								<td><input type="checkbox" name="music" value="Listening to Music"/>Listening to Music</td>		
								<td><input type="checkbox" name="hunting" value="Hunting"/>Hunting</td>
							</tr>
							<tr class="success">
								<td><input type="checkbox" name="shopping" value="Shopping"/>Shopping</td>
								<td><input type="checkbox" name="traveling" value="Traveling"/>Traveling</td>		
								<td><input type="checkbox" name="sleeping" value="Sleeping"/>Sleeping</td>
							</tr>
						</table>
						<button type="submit" name="button" value="">Submit</button>
						<button type="reset" name="button" value="Reset">Reset</button>
				</form>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
