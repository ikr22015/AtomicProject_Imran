<?php
	include_once("../../../vendor/autoload.php");
	
	use Imran\BITM\SEIP106357\ProfilePicture\FileUpload;
	use Imran\BITM\SEIP106357\Utility\Utility;
	
	$myAvatar = new FileUpload();
	$avators = $myAvatar->show($_REQUEST['id']);
	
	
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Profile Picture</title>
	<link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">Profile Picture</a></h1>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="view-profile-pic-sub-container">
				<div class="content">
					<h2>My Profile Picture</h2>
						<figure id="p-img">
							<img src="<?php data:image/jpeg;base64,'.base64_encode($image->load()) .'?>" width="200" height="200" alt="..." class="img-thumbnail">
						</figure>
					<h3 style="text-align:center"><?php echo $avators->title;?></h3>
					
					<button class="change" type="button" name="button"><a href="profile-pic-update.html">Change</a></button>
					<button class="delete" type="button" name="button"><a href="profile-pic-del.html">Delete</a></button>
				</div>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
