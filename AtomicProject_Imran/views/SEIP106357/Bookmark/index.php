<?php
	include_once("../../../vendor/autoload.php");
	
	use Imran\BITM\SEIP106357\Bookmark\Url;
	use Imran\BITM\SEIP106357\Utility\Utility;
	
	$bookmark = new Url();
	$bookmarks = $bookmark->index();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bookmark</title>
	<link rel="stylesheet" href="../../../resource/css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">Bookmark</a></h1>
			<nav class="navbar navbar-inverse">
				<ul>
					<li><a class="active" href="index.php">Home</a></li>
					<li><a href="../../../index.php">Go to Ptoject Navigation Page</a></li>
					<li><a href="#">Click here to view the source code</a></li>
				</ul>
			</nav>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="booklist-sub-container">
			<div class="list-nav">
					<ul>
						<li>
							<form action="filter.php" method="get">
								<input type="number" name="filter" placeholder="Search by ID...">
								<button type="submit" name="" value="">Search</button>
							</form>
						</li>
						<li><a href="">Download</a></li>
						<li><a href="create.php">Add New Book</a></li>
					</ul>
			</div>
				<div id="profile-sum-org-panel-success" class="panel panel-primary">
					<div id="profile-sum-org-panel-heading" class="panel-heading">Bookmark List</div>
					<div>
						<?php echo Utility::message(); ?>
					</div>
						<table class="table table-bordered" border="1">
						<tr>
							<th>SL</th>
							<th>ID</th>
							<th>Url</th>
							<th>Action</th>
						</tr>
						  <?php
							$SL = 1;
						  foreach($bookmarks as $bookmark){
						  ?>
						  <tr>
							<td><?php echo $SL; ?></td>
							<td><?php echo $bookmark->id ?></td>
							<td style="text-align:left"><a href="<?php echo $bookmark->title ?>" target="_blank"><?php echo $bookmark->title ?></a></td> 
							<td style="text-align:center">
								<input type="hidden" name="id" value="<?php echo $bookmark->id ?>">
								<button type="button" name="button" value=""><a href="delete.php?id=<?php echo $bookmark->id ?>">Delete</a></button>
							</td>
						  </tr>
						  <?php 
						  $SL++;
						  }
						  ?>
						</table>
				</div>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
