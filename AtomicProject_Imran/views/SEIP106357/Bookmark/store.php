<?php
	include_once("../../../vendor/autoload.php");
	
	use Imran\BITM\SEIP106357\Bookmark\Url;
	use Imran\BITM\SEIP106357\Utility\Utility;
	
	$bookmark = new Url($_POST);
	$bookmark->store();
?>