<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Terms And Conditions</title>
	<link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">Terms And Conditions</a></h1>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="terms-sub-container">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">Terms And Conditions</h3>
						</div>
						<div class="panel-body">
							<p>Whether indents and exdents are right for your project will depend on the content you’re working with and the other design decisions you’ve made. They’re not always a good fit, just as a line break isn’t the only ‘right’ way to set a paragraph. But we think it’s nice to have options, and now you can try them out quickly and easily before you decide.</br></br>
							</p>
							<form action="store.php" method="post">
							<input style="margin-left:386px" type="checkbox" name="terms-and-conditions" value="Agree"/><label style="margin-left:8px">I agree with these <span style="color:#337AB7">terms and conditions.</span></label><br>
							<button style="margin-left:502px" type="submit" name="button"/>Submit</button>
							</form>
						</div>
					</div>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
